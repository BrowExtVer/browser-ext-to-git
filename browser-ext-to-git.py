#!/usr/bin/env python3

import argparse
from dataclasses import dataclass
import datetime
from io import BytesIO
import logging
from pathlib import Path
import os
import re
import subprocess
import zipfile

import requests


LOGGER = logging.getLogger(__name__)


@dataclass
class Version:
    version_string: str
    version_id: str
    download_url: str
    license_name: str
    date: datetime.datetime
    changelog: str


def get_output(cmd):
    return subprocess.check_output(cmd, encoding='utf-8')


def parse_moz_entry(entry):
    return Version(
        version_id=str(entry['id']),
        version_string=entry['version'],
        license_name=entry['license']['name']['en-US'],
        download_url=entry['files'][0]['url'],
        changelog=entry['release_notes']['en-US'],
        date=datetime.datetime.strptime(
            entry['files'][0]['created'], '%Y-%m-%dT%H:%M:%SZ'
        ),
    )


def get_last_version_id():
    # if previous git commands worked but not this one
    # it's probably because there are no commits yet
    try:
        log = get_output(['git', 'log', '-10'])
    except subprocess.CalledProcessError:
        return None

    match = re.search(r'^\s*Version-ID: (\S+)$', log, flags=re.MULTILINE)
    return match[1]


class Mozilla:
    def __init__(self):
        self.requester = requests.Session()

    def metadata(self, extname):
        url = f"https://addons.mozilla.org/api/v4/addons/addon/{extname}/"
        reply = self.requester.get(url)
        if reply.status_code != 404:
            return reply.json()

    def _fetch_moz_versions(self, extname):
        url = f"https://addons.mozilla.org/api/v4/addons/addon/{extname}/versions/"
        while url:
            reply = self.requester.get(url).json()
            yield from reply['results']
            url = reply['next']

    def get_mozilla_versions(self, extname, until_id=None):
        ret = []

        for jversion in self._fetch_moz_versions(extname):
            version = parse_moz_entry(jversion)
            if until_id and until_id == version.version_id:
                break
            ret.append(version)

        # site is ordered from newest to oldest
        ret.reverse()
        return ret

    def download(self, *args, **kwargs):
        return self.requester.get(*args, **kwargs)


def main():
    argparser = argparse.ArgumentParser(
        description='Mirror a web-extension version history to a git repository'
    )
    argparser.add_argument(
        '--mozilla',
        metavar='NAME',
        help='Name of mozilla extension',
    )
    argparser.add_argument('--output', type=Path, metavar='DIR')
    args = argparser.parse_args()

    if not args.mozilla:
        argparser.error('need to pass --mozilla')
    # TODO other browsers

    moz = Mozilla()
    if not moz.metadata(args.mozilla):
        argparser.error(f"extension {args.mozilla!r} doesn't exist")

    if not args.output:
        args.output = Path(args.mozilla)

    if not args.output.is_dir():
        args.output.mkdir()

    os.chdir(args.output)

    # git-init(1) does not overwrite an existing repository
    subprocess.check_call(['git', 'init'])
    subprocess.check_call(['git', 'config', 'user.name', 'nobody'])
    subprocess.check_call(['git', 'config', 'user.email', 'nobody@localhost'])

    until = get_last_version_id()

    versions = moz.get_mozilla_versions(args.mozilla, until)

    for version in versions:
        LOGGER.info('adding version %s', version.version_string)

        # we want only snapshots:
        # we don't want spurious files from previous snapshots
        # so remove all files and add current
        subprocess.call(['git', 'rm', '-qr', '.'])

        zipreply = moz.download(version.download_url)
        zipobj = zipfile.ZipFile(BytesIO(zipreply.content))
        for zipentry in zipobj.infolist():
            zipobj.extract(zipentry)
            subprocess.check_call(['git', 'add', zipentry.filename])

        commit_message = '\n'.join([
            f"version {version.version_string}",
            '',
            version.changelog,
            '',
            f"Version-ID: {version.version_id}"
        ])

        subprocess.check_call([
            'git', 'commit', '--date', version.date.isoformat(),
            '--message', commit_message,
        ])
        subprocess.call([
            'git', 'tag', '-a', version.version_string,
            '--message', commit_message,
        ])


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
